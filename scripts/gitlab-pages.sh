#!/usr/bin/env sh

# This script performs all the necessary steps to deploy this module on Gitlab Pages

set -e

npm install --frozen-lockfile
npm run build

rm -rf public
mv build public
