#!/usr/bin/env sh

# This script performs all the necessary steps to build and test this module.
# You can use it in continuous integration setups.

set -e

npm ci
npm run lint
npm run build
npm run test

echo "It works 💪 (hope so)"
