/// <reference types="@abc-map/module-api/build/typings" />

declare const RANDOM_SUFFIX: string;

declare module '*.png' {
  const content: string;
  export default content;
}

declare module '*.jpg' {
  const content: string;
  export default content;
}

declare module '*.jpeg' {
  const content: string;
  export default content;
}

declare module '*.svg' {
  const content: string;
  export default content;
}

declare module '*.css' {
  const content: string;
  export default content;
}

declare module '*.module.scss' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare module '*.json' {
  const content: any;
  export default content;
}

declare module '*.mp4' {
  const content: any;
  export default content;
}
