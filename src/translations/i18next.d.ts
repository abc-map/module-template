import 'i18next';

// This file correct typings according to selected options
declare module 'i18next' {
  interface CustomTypeOptions {
    returnNull: false;
  }
}
