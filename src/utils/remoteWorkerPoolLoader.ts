import * as Comlink from 'comlink';
import { ModuleWorker } from '../worker';
import { remoteWorkerLoader, WorkerContext } from './remoteWorkerLoader';
import { Logger } from '@abc-map/module-api';

const logger = Logger.get('remoteWorkerPoolLoader.ts', 'info');

export interface WorkerPoolContext {
  execute: <T = void>(task: Task<T>) => T;
  dispose: () => void;
}

export type Task<T = any> = (worker: Comlink.Remote<ModuleWorker>) => T;

export type WorkerPoolLoader = () => Promise<WorkerPoolContext>;

export async function remoteWorkerPoolLoader(_concurrency?: number): Promise<WorkerPoolContext> {
  // Concurrency parameters
  const concurrency = _concurrency ?? Math.max(navigator.hardwareConcurrency - 2, 2);
  if (concurrency < 1) {
    throw new Error(`Invalid concurrency: ${concurrency}`);
  }
  logger.info(`Using ${concurrency} workers`);

  // Worker instanciation
  const workers: WorkerContext[] = [];
  const loading: Promise<any>[] = [];
  for (let i = 0; i < concurrency; i++) {
    loading.push(remoteWorkerLoader().then((worker) => workers.push(worker)));
  }
  await Promise.all(loading);

  // Execute function, round robin
  let i = 0;
  const execute = (task: Task) => {
    i++;
    if (i === workers.length) i = 0;
    return task(workers[i].remote);
  };

  // Dispose function
  const dispose = () => {
    workers.forEach((w) => w.dispose());
  };

  return { execute, dispose };
}
