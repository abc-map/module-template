import * as sinon from 'sinon';
import { SinonStub, SinonStubbedInstance } from 'sinon';
import { Task, WorkerPoolLoader } from './remoteWorkerPoolLoader';
import { ModuleWorker } from '../worker';

export interface TestWorkerPoolContext {
  execute: SinonStub<[(t: Task) => void]>;
  dispose: SinonStub<[], void>;
  worker: SinonStubbedInstance<ModuleWorker>;
}

export function testRemoteWorkerPoolLoader(): [TestWorkerPoolContext, WorkerPoolLoader] {
  const worker = sinon.createStubInstance(ModuleWorker);

  const context: TestWorkerPoolContext = {
    execute: sinon.stub(),
    dispose: sinon.stub(),
    worker,
  };

  context.execute.callsFake((task) => Promise.resolve(task(worker as any)));

  return [context, (() => Promise.resolve(context)) as unknown as WorkerPoolLoader];
}
