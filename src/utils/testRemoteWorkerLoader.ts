import * as sinon from 'sinon';
import { ModuleWorker } from '../worker';
import { SinonStub, SinonStubbedInstance } from 'sinon';
import { WorkerLoader } from './remoteWorkerLoader';

export interface TestWorkerContext {
  remote: SinonStubbedInstance<ModuleWorker>;
  dispose: SinonStub;
}

export function testRemoteWorkerLoader(): [TestWorkerContext, WorkerLoader] {
  const context: TestWorkerContext = {
    remote: sinon.createStubInstance(ModuleWorker),
    dispose: sinon.stub(),
  };

  return [context, (() => Promise.resolve(context)) as unknown as WorkerLoader];
}
