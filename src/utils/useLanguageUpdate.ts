import { useEffect } from 'react';
import { setLang } from '../translations/i18n';
import { Language, Logger } from '@abc-map/module-api';

const logger = Logger.get('useLanguageUpdate.ts');

// Update module language when user changes application language
export function useLanguageUpdate(lang: Language | undefined) {
  useEffect(() => {
    setLang(lang || Language.English).catch((err) => logger.error('Language error:', err));
  }, [lang]);
}
