import * as Comlink from 'comlink';
import { ModuleWorker } from '../worker';

export interface WorkerContext {
  remote: Comlink.Remote<ModuleWorker>;
  dispose: () => void;
}

export type WorkerLoader = () => Promise<WorkerContext>;

/**
 * This function helps to load worker with a relative path but a different origin.
 */
export const remoteWorkerLoader: WorkerLoader = async function (): Promise<WorkerContext> {
  if (!moduleApi.resourceBaseUrl) {
    return Promise.reject(new Error('resourceBaseUrl not set'));
  }

  // We fetch worker code based on resourceBaseUrl, which contains the public URL of worker
  const url = `${moduleApi.resourceBaseUrl}/worker.js`;
  let workerCode = await fetch(url).then((res) => res.text());

  // We fix source maps, we must use public worker URL too
  workerCode = workerCode.replace('//# sourceMappingURL=worker.js.map', `\n//# sourceMappingURL=${url}.map\n`);
  const script = 'let exports = {};' + workerCode;
  const scriptURL = URL.createObjectURL(new Blob([script]));

  // We load worker then return it
  const worker = new Worker(scriptURL);
  const remote = Comlink.wrap<ModuleWorker>(worker);
  return {
    remote,
    dispose: () => {
      URL.revokeObjectURL(scriptURL);
    },
  };
};
