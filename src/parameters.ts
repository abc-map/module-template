import { FeatureCollection } from '@turf/helpers';
import { Type } from 'ol/geom/Geometry';

/**
 * These parameters are needed for processing
 */
export interface Parameters {
  layerId: string;
  bufferSize: number;
  units: 'meters' | 'kilometers';
  geometrySelection: GeometrySelection[];
}

export type GeometrySelection = 'points' | 'lines' | 'polygons';

/**
 * This function return the default parameters, used if no parameters selected
 */
export function newDefaultParameters(): Parameters {
  return {
    layerId: '',
    bufferSize: 15,
    units: 'kilometers',
    geometrySelection: ['points', 'lines', 'polygons'],
  };
}

/**
 * When processing done, module we return this object to UI
 */
export interface Result {
  errors: string[];
}

/**
 * Using the worker, you can pass progress events to UI in order to inform user.
 */
export interface ProcessingProgressEvent {
  total: number;
  current: number;
}

/**
 * Worker processing internal parameters
 */
export interface BufferParameters {
  bufferSize: number;
  units: 'meters' | 'kilometers';
  collection: FeatureCollection;
}

export function geometrySelectionToTypes(selection: GeometrySelection[]): Type[] {
  const result: Type[] = [];
  if (selection.includes('points')) {
    result.push('Point', 'MultiPoint');
  }
  if (selection.includes('lines')) {
    result.push('LineString', 'MultiLineString');
  }
  if (selection.includes('polygons')) {
    result.push('Polygon', 'MultiPolygon');
  }

  return result;
}
