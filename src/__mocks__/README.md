# __mocks__

This directory contains mocks, simulated objects that mimic the behavior of real objects of files.

You can create files manually and import them in `jest.config.js`, or use [jest](https://jestjs.io/docs/26.x/es6-class-mocks)

