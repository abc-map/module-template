import { Feature } from 'ol';
import { Point } from 'ol/geom';

export function sampleOpenlayersFeatures(nbr = 15): Feature[] {
  const features: Feature[] = [];
  for (let i = 0; i < nbr; i++) {
    const feat = new Feature();
    feat.setId(i);
    feat.setGeometry(new Point([i, i]));
    features.push(feat);
  }

  return features;
}
