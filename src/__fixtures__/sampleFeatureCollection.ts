import { FeatureCollection } from '@turf/helpers';

export function sampleFeatureCollection(): FeatureCollection {
  return {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [-9973161.717428254, 2834762.6219614227],
        },
        properties: {
          'abc:style:point:icon': 'twbs/geo-alt-fill.inline.svg',
          'abc:style:point:size': 30,
          'abc:style:point:color': 'rgba(18,90,147,0.9)',
          'abc:feature:selected': false,
        },
        id: 'N62emxq3N6',
      },
      {
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: [
            [-9709101.717428254, 2866547.6219614227],
            [-9824016.717428254, 2680727.6219614227],
            [-9486606.717428254, 2607377.6219614227],
            [-9711546.717428254, 2521802.6219614227],
            [-9584406.717428254, 2284637.6219614227],
          ],
        },
        properties: {
          'abc:style:stroke:color': 'rgba(18,90,147,0.60)',
          'abc:style:stroke:width': 3,
          'abc:feature:selected': false,
        },
        id: 'TDsKocLf1k',
      },
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [-9068511.717428254, 2558477.6219614227],
              [-9264111.717428254, 2362877.6219614227],
              [-8982936.717428254, 2191727.6219614227],
              [-8809341.717428254, 2375102.6219614227],
              [-8809341.717428254, 2636717.6219614227],
              [-9068511.717428254, 2558477.6219614227],
            ],
          ],
        },
        properties: {
          'abc:style:stroke:color': 'rgba(18,90,147,0.60)',
          'abc:style:stroke:width': 3,
          'abc:style:fill:color1': 'rgba(18,90,147,0.30)',
          'abc:style:fill:color2': 'rgba(255,255,255,0.60)',
          'abc:style:fill:pattern': 'abc:style:fill:hatching:oblique:right',
          'abc:feature:selected': true,
        },
        id: 't0hBdOywVx',
      },
    ],
  };
}
