import path from 'path';
import webpack from 'webpack';
import CopyPlugin from 'copy-webpack-plugin';
import {customAlphabet} from 'nanoid';
import url from "url";

const {DefinePlugin} = webpack;
const nanoid = customAlphabet('1234567890abcdefghijklmnopqrstuvwxyz', 10);

export default function () {
  const rootDir = url.fileURLToPath(new URL('.', import.meta.url));

  return {
    entry: {
      module: './src/index.ts',
      worker: './src/worker.ts',
    },
    devtool: 'source-map',
    output: {
      path: path.resolve(rootDir, 'build'),
      filename: '[name].js',
      library: {
        name: 'module',
        type: 'commonjs',
      },
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    externals: {
      react: 'window react',
    },
    plugins: [
      new CopyPlugin({patterns: [{from: 'public'}]}),
      // This suffix is used in module name
      new DefinePlugin({RANDOM_SUFFIX: JSON.stringify(nanoid())}),
    ],
    module: {
      rules: [
        {
          test: /\.(tsx|ts)$/,
          include: path.resolve(rootDir, 'src'),
          exclude: [/node_modules/, new RegExp('.test.tsx?')],
          use: [
            {
              loader: 'babel-loader',
            },
          ],
        },
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: {
                  auto: true,
                  mode: 'local',
                  localIdentName: '[name]__[local]--[hash:base64:5]',
                },
              },
            },
            'sass-loader',
          ],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif|mp4)$/i,
          type: 'asset/resource',
        },
      ],
    },
  };
};
