# This file allow to load CI environment variable for debug purposes
# Usage: "cd abc-map && source ci-env.sh"

export PROJECT_DIR=`pwd`

export CI="true"
npm config set cache "$PROJECT_DIR/.npm-cache"


